local srcdir = arg[1] or "."

public_headers = {
    "lprefix.h",
    "luaconf.h",
    "lua.h",
    "lauxlib.h",
    "lualib.h",
}

private_headers = {
    "llimits.h",
    "lapi.h",
    "lobject.h",
    "lmem.h",
    "lzio.h",
    "llex.h",
    "lopcodes.h",
    "lparser.h",
    "lcode.h",
    "ltm.h",
    "lstate.h",
    "lctype.h",
    "ldebug.h",
    "ldo.h",
    "lfunc.h",
    "lgc.h",
    "lstring.h",
    "ltable.h",
    "lundump.h",
    "lvm.h",
}

core_sources = {
    "lapi.c",
    "lcode.c",
    "lctype.c",
    "ldebug.c",
    "ldo.c",
    "ldump.c",
    "lfunc.c",
    "lgc.c",
    "llex.c",
    "lmem.c",
    "lobject.c",
    "lopcodes.c",
    "lparser.c",
    "lstate.c",
    "lstring.c",
    "ltable.c",
    "ltm.c",
    "lundump.c",
    "lvm.c",
    "lzio.c",
}

lib_sources = {
    "lauxlib.c",
    "lbaselib.c",
    "lbitlib.c",
    "lcorolib.c",
    "ldblib.c",
    "linit.c",
    "liolib.c",
    "lmathlib.c",
    "loadlib.c",
    "loslib.c",
    "lstrlib.c",
    "ltablib.c",
    "lutf8lib.c",
}

luac_sources = {
    "luac.c",
}

lua_sources = {
    "lua.c",
}

local prefix = [[
/*
** lua-all.c -- All of Lua in a single file.
**
** Lua version: %s
** Created at: %s
**
** Examples for MinGW:
**
**   > gcc -DLUA_BUILD_AS_DLL -DLIBLUA_IMPLEMENTATION -DMAKE_LIBLUA -shared -o lua.dll lua-all.c
**   > gcc -DMAKE_LIBLUA -o liblua.o lua-all.c && ar rcs liblua.a liblua.o
**   > gcc -DMAKE_LUA -o lua lua-all.c
**   > gcc -DMAKE_LUAC -o luac lua-all.c
**
** Example for linux:
**
**   $ gcc -o lua -DMAKE_LUA -DLUA_USE_POSIX lua-all.c
**   $ gcc -o luac -DMAKE_LUAC -DLUA_USE_POSIX lua-all.c
**   $ gcc -o lua.so -shared -fPIC -DMAKE_LIBLUA -DLUA_USE_POSIX lua-all.c -lm
**
** Defines:
**
**   MAKE_LUA   Build the `lua` executable. When this is defined, do not define
**              any of the other defines. This is the default if no other
**              defines are set.
**
**   MAKE_LUAC  Build the `luac` executable. See `MAKE_LUA`.
**
**   MAKE_LIBLUA  Build the `liblua` shared/static library. To build a DLL on
**                Windows, remember to define `LIBLUA_IMPLEMENTATION` and
**                `LUA_BUILD_AS_DLL`.
**
**   LIBLUA_IMPLEMENTATION  Include all of the code. If you define this, you
**                          should not define `LIBLUA_INTERFACE`. You need to
**                          define this if compiling the file as a DLL to make
**                          the exported functions marked with correct
**                          visibility. You can also define this before
**                          including this file from some other source file to
**                          include the implementation code.
**
**   LIBLUA_INTERFACE  Include only the public api. This cannot be defined if
**                     `LIBLUA_IMPLEMENTATION` is defined.
**
*/

#if !defined(AMALG_INCLUDE_CORE) && \
    !defined(AMALG_INCLUDE_LIB) && \
    !defined(AMALG_INCLUDE_LUA) && \
    !defined(AMALG_INCLUDE_LUAC)
# if !defined(MAKE_LUA) && !defined(MAKE_LUAC) && !defined(MAKE_LIBLUA) && \
     !defined(LIBLUA_INTERFACE)
#   define MAKE_LUA
# endif
#endif

#if defined(MAKE_LUA)
# define AMALG_PUBLIC_HEADERS
# define AMALG_PRIVATE_HEADERS
# define AMALG_CORE_SOURCES
# define AMALG_LIB_SOURCES
# define AMALG_LUA_SOURCES
#elif defined(MAKE_LUAC)
# define AMALG_PUBLIC_HEADERS
# define AMALG_PRIVATE_HEADERS
# define AMALG_CORE_SOURCES
# define AMALG_LUAC_SOURCES
#elif defined(MAKE_LIBLUA)
# define AMALG_PUBLIC_HEADERS
# define AMALG_PRIVATE_HEADERS
# define AMALG_CORE_SOURCES
# define AMALG_LIB_SOURCES
#endif

#if defined(LIBLUA_INTERFACE)
# define AMALG_PUBLIC_HEADERS
#elif defined(LIBLUA_IMPLEMENTATION)
# define LUA_CORE
# define LUA_LIB
#endif

#if defined(AMALG_INCLUDE_CORE)
# define AMALG_PUBLIC_HEADERS
# define AMALG_PRIVATE_HEADERS
# define AMALG_CORE_SOURCES
#elif defined(AMALG_INCLUDE_LIB)
# define AMALG_PUBLIC_HEADERS
# define AMALG_PRIVATE_HEADERS
# define AMALG_LIB_SOURCES
#elif defined(AMALG_INCLUDE_LUA)
# define AMALG_PUBLIC_HEADERS
# define AMALG_PRIVATE_HEADERS
# define AMALG_LUA_SOURCES
#elif defined(AMALG_INCLUDE_LUAC)
# define AMALG_PUBLIC_HEADERS
# define AMALG_PRIVATE_HEADERS
# define AMALG_LUAC_SOURCES
#endif

]]

-- Returns a coroutine that yields lines from an open file.
local function read_lines(file)
    return coroutine.create(function()
        while true do
            local line = file:read("L")
            coroutine.yield(line)
        end
    end)
end

-- Returns a coroutine that reads lines from producer and yields lines that are
-- not local includes.
local function strip_local_includes(producer)
    return coroutine.create(function()
        while true do
            local _, line = coroutine.resume(producer)
            if not line or (not line:match([[^#include%s*"]])) then
                coroutine.yield(line)
            end
        end
    end)
end

-- Consumer that reads lines from input producer and writes the lines to
-- output file. Output should be a file opened for writing.
local function write_lines(output, input)
    while true do
        local result, line = coroutine.resume(input)
        if line then
            output:write(line)
        else
            break
        end
    end
end

-- Returns a coroutine that yields the file information preprocessor directive
-- as the first line and then proceeds to yield lines from producer.
local function include_file_info(filename, producer)
    return coroutine.create(function()
        coroutine.yield(string.format("#line 1 \"%s\"\n", filename))
        while true do
            local _, line = coroutine.resume(producer)
            coroutine.yield(line)
        end
    end)
end

-- Returns a coroutine that wraps the lines from producer in an ifdef/ifndef
-- section. If negative is not false-ish, then an #ifndef is generated instead
-- of #ifdef.
local function ifdef(guard, producer, negative)
    local token

    if negative then
        token = "ifndef"
    else
        token = "ifdef"
    end

    return coroutine.create(function()
        coroutine.yield(string.format("#%s %s\n", token, guard))
        while true do
            local _, line = coroutine.resume(producer)
            if not line then
                break
            end
            coroutine.yield(line)
        end
        coroutine.yield(string.format("#endif /* %s %s */\n", token, guard))
        coroutine.yield(nil)
    end)
end

-- Returns a coroutine that yields all lines (with file info included) from all
-- of the files listed in the `files` array.
local function read_files(files)
    return coroutine.create(function()
        for _, file in ipairs(files) do
            local input = assert(io.open(srcdir .. "/" .. file, "r"))
            local producer = include_file_info(file, read_lines(input))
            while true do
                local _, line = coroutine.resume(producer)
                if not line then
                    break
                end
                coroutine.yield(line)
            end
            input:close()
        end
        coroutine.yield(nil)
    end)
end

local output = io.open("lua-all.c", "w")

output:write(string.format(prefix,
    os.getenv("LUA_VERSION"),
    os.date("!%Y-%m-%dT%H:%M:%SZ")))

write_lines(output, ifdef("AMALG_PUBLIC_HEADERS", strip_local_includes(read_files(public_headers))))

write_lines(output, ifdef("AMALG_PRIVATE_HEADERS", strip_local_includes(read_files(private_headers))))

write_lines(output, ifdef("AMALG_CORE_SOURCES", strip_local_includes(read_files(core_sources))))

write_lines(output, ifdef("AMALG_LIB_SOURCES", strip_local_includes(read_files(lib_sources))))

write_lines(output, ifdef("AMALG_LUA_SOURCES", strip_local_includes(read_files(lua_sources))))

write_lines(output, ifdef("AMALG_LUAC_SOURCES", strip_local_includes(read_files(luac_sources))))

-- write_lines(output, ifdef("MAKE_LUAC", strip_local_includes(read_files(
--     luac_sources))))

-- output:write(string.format(prefix,
--     os.getenv("LUA_VERSION"),
--     os.date("!%Y-%m-%dT%H:%M:%SZ")))

-- write_lines(output, strip_local_includes(read_files(public_headers)))

-- output:write[[
-- #ifndef LIBLUA_INTERFACE
-- ]]

-- write_lines(output, strip_local_includes(read_files(private_headers)))

-- write_lines(output, strip_local_includes(read_files(common_sources)))

-- write_lines(output, ifdef("MAKE_LUAC", strip_local_includes(read_files(
--     liblua_sources)), true))

-- write_lines(output, ifdef("MAKE_LUA", strip_local_includes(read_files(
--     lua_sources))))

-- write_lines(output, ifdef("MAKE_LUAC", strip_local_includes(read_files(
--     luac_sources))))

-- output:write[[
-- #endif /* LIBLUA_INTERFACE */
-- ]]

output:close()
