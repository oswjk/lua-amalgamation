#!/bin/sh

export LUA_VERSION=5.3.2
LUA_PLATFORM=linux
LUA_NAME=lua-${LUA_VERSION}
LUA_PACKAGE=${LUA_NAME}.tar.gz
LUA_URL=http://www.lua.org/ftp/${LUA_PACKAGE}

if [ ! -f $LUA_PACKAGE ]; then
    curl -R -o "$LUA_PACKAGE" "$LUA_URL"
fi

if [ ! -d $LUA_NAME ]; then
    tar xzf "$LUA_PACKAGE"
fi

if [ ! -f ${LUA_NAME}/src/lua ]; then
    make -C ${LUA_NAME} $LUA_PLATFORM
fi

./${LUA_NAME}/src/lua amalgamate.lua ${LUA_NAME}/src
